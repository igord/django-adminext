# -*- coding: utf-8 -*-

from django.contrib import admin
from django.db.models.loading import get_model
from django.forms import widgets
from django.utils.functional import update_wrapper
from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse, resolve
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.contrib.admin.util import unquote
from django.utils.safestring import mark_safe
from django.utils.encoding import force_unicode
from django.utils.http import urlencode
from django.utils.html import escape, escapejs
from django.http import HttpResponse, HttpResponseRedirect

pc_registry = {}

class ModelAdminExt(admin.ModelAdmin):
    '''
    Admin klasa za modele koji imaju vezane stavke.
    '''
    
    freeze_fields = ()
    
    def _encode_freeze_fields(self, obj):
        '''
        Enkodovanje sadržaja polja koja su navedena u
        freeze_fields na nivou AdminModel-a
        u cilju zadržavanja njihovih vrednosti prilikom prelaska
        na sledeći slog za dodavanje.
        '''
        fields = {}
        for field_name in self.freeze_fields:
            if hasattr(obj, field_name) and getattr(obj, field_name) is not None:
                field_value = obj.serializable_value(field_name)
                fields[field_name] = field_value
        if fields:
            return "?%s" % urlencode(fields)
        else:
            return ""
        
    def response_add(self, request, obj, post_url_continue='../%s/'):
        """
        Determines the HttpResponse for the add_view stage.
        
        I.D.
        Dodato čuvanje vrednosti polja navedenih u freeze_fields listi
        prilikom uzastopnog dodavanja više slogova.
        """
        opts = obj._meta
        pk_value = obj._get_pk_val()

        msg = _('The %(name)s "%(obj)s" was added successfully.') % {'name': force_unicode(opts.verbose_name), 'obj': force_unicode(obj)}
        # Here, we distinguish between different save types by checking for
        # the presence of keys in request.POST.
        if "_continue" in request.POST:
            self.message_user(request, msg + ' ' + _("You may edit it again below."))
            if "_popup" in request.POST:
                post_url_continue += "?_popup=1"
            return HttpResponseRedirect(post_url_continue % pk_value)

        if "_popup" in request.POST:
            return HttpResponse('<script type="text/javascript">opener.dismissAddAnotherPopup(window, "%s", "%s");</script>' % \
                # escape() calls force_unicode.
                (escape(pk_value), escapejs(obj)))
        elif "_addanother" in request.POST:
            self.message_user(request, msg + ' ' + (_("You may add another %s below.") % force_unicode(opts.verbose_name)))
            return HttpResponseRedirect(request.path + self._encode_freeze_fields(obj))
        else:
            self.message_user(request, msg)

            # Figure out where to redirect. If the user has change permission,
            # redirect to the change-list page for this object. Otherwise,
            # redirect to the admin index.
            if self.has_change_permission(request, None):
                post_url = '../'
            else:
                post_url = '../../../'
            return HttpResponseRedirect(post_url)

    def response_change(self, request, obj):
        """
        Determines the HttpResponse for the change_view stage.
        """
        opts = obj._meta

        # Handle proxy models automatically created by .only() or .defer()
        verbose_name = opts.verbose_name
        if obj._deferred:
            opts_ = opts.proxy_for_model._meta
            verbose_name = opts_.verbose_name

        pk_value = obj._get_pk_val()

        msg = _('The %(name)s "%(obj)s" was changed successfully.') % {'name': force_unicode(verbose_name), 'obj': force_unicode(obj)}
        if "_continue" in request.POST:
            self.message_user(request, msg + ' ' + _("You may edit it again below."))
            if "_popup" in request.REQUEST:
                return HttpResponseRedirect(request.path + "?_popup=1")
            else:
                return HttpResponseRedirect(request.path)
        elif "_saveasnew" in request.POST:
            msg = _('The %(name)s "%(obj)s" was added successfully. You may edit it again below.') % {'name': force_unicode(verbose_name), 'obj': obj}
            self.message_user(request, msg)
            return HttpResponseRedirect("../%s/" % pk_value)
        elif "_addanother" in request.POST:
            self.message_user(request, msg + ' ' + (_("You may add another %s below.") % force_unicode(verbose_name)))
            return HttpResponseRedirect("../add/" + self._encode_freeze_fields(obj))
        else:
            self.message_user(request, msg)
            # Figure out where to redirect. If the user has change permission,
            # redirect to the change-list page for this object. Otherwise,
            # redirect to the admin index.
            if self.has_change_permission(request, None):
                return HttpResponseRedirect('../')
            else:
                return HttpResponseRedirect('../../../')

    def _validate(self):
        if hasattr(self, 'child_links'):
            for link in self.child_links:
                child_model = get_model(*(link[0].split('.')))
                if not child_model:
                    raise ImproperlyConfigured(\
                        'In "%s.child_links". Model "%s" does not exists.' % \
                            (self.__class__, link[0]))
                field_name = link[1]
                if not hasattr(child_model, field_name):
                    raise ImproperlyConfigured(\
                        'In "%s.child_links". Model "%s" does not exists.' % \
                            (self.__class__, link[0]))
                    
    def __init__(self, model, admin_site):
        super(ModelAdminExt, self).__init__(model, admin_site)
        self._validate()
        
        # Pravimo registar child-parent veza "na gore"
        # Ovo je potrebno za breadcrumbs navigaciju
        if hasattr(self, 'child_links'):
            for link in self.child_links:
                # Validacija
                child_parents_link = pc_registry.setdefault(link[0], [])
                parent = ('%s.%s' % \
                    (self.model._meta.app_label, \
                    self.model.__name__), link[1])
                # Dodajemo link samo ako već ne postoji
                for p in child_parents_link:
                    if p[0]==parent[0] and p[1]==parent[1]:
                        break
                else:
                    child_parents_link.append(parent)
            if not "child_links_field" in self.list_display:
                self.list_display.append("child_links_field")
            
    def get_urls(self):
        from django.conf.urls.defaults import patterns, url

        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)
            return update_wrapper(wrapper, view)

        info = self.model._meta.app_label, self.model._meta.module_name
        urls = patterns('', 
            url(r'^(?P<parent_id>\d+)/(?P<child_app_label>\w+)/'+ \
                '(?P<field_name>\w+)/(?P<child_model_name>\w+)/(?P<next>.*)$',\
                wrap(self.parent_context), name='%s_%s_parent_context' %\
                    info),
            )
    
        
        return urls + super(ModelAdminExt, self).get_urls()    

    def parent_context(self, request, parent_id, child_app_label, field_name, \
        child_model_name, next):
        child_model = get_model(child_app_label, child_model_name)
        parent = self.model.objects.get(pk=parent_id)
                
        child_admin = self._child_admin_for_model(parent, child_model, field_name)
        view, args, kwargs = resolve('/%s' % next, \
            child_admin(child_model, admin.site))
        
        parent_context = {
            'parent' : parent,
            'field_name': field_name,
        }
        
        kwargs.update({
            'parent_context': parent_context,
        })

        return view(request, *args, **kwargs)
        
    def changelist_view(self, request, parent_context=None, extra_context=None):
        extra_context = extra_context or {}
        if parent_context:
            parent = parent_context['parent']
            field_name = parent_context['field_name']
            breadcrumbs = self.breadcrumbs(request, parent, field_name, \
                appending=False)
            extra_context.update({ 'parent': parent , \
                'breadcrumbs': breadcrumbs, \
                'has_breadcrumbs': True, \
             })

        return super(ModelAdminExt, self).changelist_view(request, \
             extra_context=extra_context)

    def change_view(self, request, child_id, parent_context=None, \
        extra_context=None):
        if extra_context is None:
            extra_context = {}
        if parent_context:
            parent = parent_context['parent']
            field_name = parent_context['field_name']
            
            extra_context['has_breadcrumbs'] = True
            if not extra_context.has_key('breadcrumbs'):
                breadcrumbs = self.breadcrumbs(request, parent, field_name, \
                    child_id, appending=True)
                breadcrumbs.append([[_('Change'), '']])
                extra_context['breadcrumbs'] = breadcrumbs
                

            if not extra_context.has_key('title'):
                extra_context['title'] =  \
                    _('%(child)s for %(parent_name)s "%(parent)s"') % \
                    {
                        'child': self.model._meta.verbose_name,
                        'parent_name': parent._meta.verbose_name.lower(), 
                        'parent': unicode(parent)
                    }
            
            extra_context['parent'] = parent

        obj = self.get_object(request, unquote(child_id))
        extra_context['child_links'] = mark_safe(self.child_links_field(obj))
        
        return super(ModelAdminExt, self).change_view(request, \
            child_id, extra_context=extra_context)
        

    def add_view(self, request, parent_context=None, extra_context=None):
        if extra_context is None:
            extra_context = {}
        if parent_context:
            parent = parent_context['parent']
            field_name = parent_context['field_name']
            
            extra_context['has_breadcrumbs'] = True
            if not extra_context.has_key('breadcrumbs'):
                breadcrumbs = self.breadcrumbs(request, parent, field_name, \
                    appending=True)
                breadcrumbs.append([[_('Add'), '']])
                extra_context['breadcrumbs'] = breadcrumbs
            
            if not extra_context.has_key('title'):
                extra_context['title'] =  \
                    _('%(child)s for %(parent_name)s "%(parent)s"') % \
                    {
                        'child': self.model._meta.verbose_name,
                        'parent_name': parent._meta.verbose_name.lower(), 
                        'parent': unicode(parent)
                    }
                
            extra_context['parent'] = parent

        return super(ModelAdminExt, self).add_view(request, \
            extra_context=extra_context)


    def history_view(self, request, child_id, parent_context=None):
        if parent_context:
            parent = parent_context['parent']
            field_name = parent_context['field_name']
            breadcrumbs = self.breadcrumbs(request, parent, field_name, \
                child_id, appending=True)
            breadcrumbs.append([[_('History'), '']])
            extra_context={ 'parent': parent  , \
                'breadcrumbs': breadcrumbs, \
                'has_breadcrumbs': True}
        else:
            extra_context = None
        
        return super(ModelAdminExt, self).history_view(request, \
            child_id, extra_context=extra_context)
        


    def delete_view(self, request, child_id, parent_context=None):
        if parent_context:
            parent = parent_context['parent']
            field_name = parent_context['field_name']
            breadcrumbs = self.breadcrumbs(request, parent, field_name, \
                child_id, appending=True)
            breadcrumbs.append([[_('Delete'), '']])
            extra_context={ 'parent': parent  , \
                'breadcrumbs': breadcrumbs, \
                'has_breadcrumbs': True}
        else:
            extra_context = None

        return super(ModelAdminExt, self).delete_view(request, \
            child_id, extra_context=extra_context )
        

    def child_links_field(self, obj):
        '''
        Veza sa child elementima
        child_links je tuple tupleova admin klase oblika 
        (app_label.ChildModel, 'field_name', 'css class name').
        Obavezno je navesti prva dva parametra.
        '''
        anchor = ''
        for link in getattr(self, 'child_links', []):
            child_model_fullname, field_name = link[0:2]
            splited_name = child_model_fullname.split('.')
            assert len(splited_name)==2, '''child_links first parameter must 
                be in the form 'app_name.model_name" in admin class %s''' %\
                    unicode(self)
            
            child_model_app, child_model_name = splited_name
            child_model = get_model(child_model_app, child_model_name)
            parent_model_name = obj._meta.module_name
            app_label = obj._meta.app_label
            if len(link)>2:
                css_class = "child_items %s" % link[2]
            else:
                css_class = "child_items %s" % child_model_name.lower() 
                
            anchor += '<a href="%s" class="%s" title="%s"></a>'\
                % (reverse("admin:%s_%s_parent_context" %\
                    (app_label, parent_model_name),
                    kwargs={'parent_id': obj.id, \
                            'child_app_label': child_model._meta.app_label, \
                            'field_name': field_name, \
                            'child_model_name': child_model_name.lower(),
                            'next':''}),
                    css_class, unicode(child_model._meta.verbose_name_plural))
        return anchor
    child_links_field.short_description = _(u'Child items')
    child_links_field.allow_tags = True


    def _child_admin_for_model(self, parent, child_model, field_name):
        ''' 
        Kreira admin klasu za child elemente.
        Zadaje se child model i naziv polja koje povezuje child za parent.
        '''
        admin_class_instance = admin.site._registry[child_model]
        admin_class = admin_class_instance.__class__
            
        # Kreiranje admin klase za child modele koja se nasleđuje iz osnovne
        # admin child klase.
        class ChildAdmin(admin_class):
            class Media:
                js = (
                    '%sadminext/adminext/adminext.js' % settings.STATIC_URL,
                )

                    
            list_display = list(admin_class.list_display)
            list_display_links = list(admin_class.list_display_links)
            
            if field_name in list_display:
                list_display.remove(field_name)
            if field_name in list_display_links:
                list_display_links.remove(field_name)

            def get_form(self, request, obj=None, **kwargs):
                """
                Returns a Form class for use in the admin add view. This is used by
                add_view and change_view.
                """
                form = super(ChildAdmin, self).get_form(request, obj, **kwargs)
                field = form.base_fields[field_name]
                field.widget = widgets.HiddenInput()
                field.widget.attrs.update({ 'class': 'hidden-form-field'})
                field.initial = parent.id
                return form

            def queryset(self, request):
                qs = super(ChildAdmin, self).queryset(request)
                return qs.filter(**{str(field_name):parent})
            
            @property
            def urlpatterns(self):
                return self.get_urls()


        return ChildAdmin

    def breadcrumbs(self, request, parent, field_name, child_id=None, appending=False):
        '''
        Kreiranje breadcrumbs navigacije.
        '''
        
        breadcrumbs = []
        parent_app_label = parent._meta.app_label
        parent_model_name = parent.__class__.__name__
        child_app_label = self.model._meta.app_label
        child_model_name = self.model._meta.module_name
        parent_fqn = '%s.%s' % (parent_app_label, parent_model_name)
        parent_model = parent.__class__
        
        
        # listview-ovi za tekućeg parent-a
        pparent_bc = []
        if parent.id and pc_registry.has_key(parent_fqn): # Ako imamo parent of a parent
            pparent_links = pc_registry.get(parent_fqn, [])
            for pparent_link in pparent_links:
                pparent_app_label, pparent_model_name = pparent_link[0].split('.')
                pparent = getattr(parent, pparent_link[1])
                if pparent:
                    pparent_url_link = reverse('admin:%s_%s_parent_context' %\
                        (pparent_app_label, pparent_model_name.lower()), \
                        kwargs={'parent_id': pparent.id, \
                                'child_app_label': parent_app_label, \
                                'field_name': pparent_link[1], \
                                'child_model_name': parent_model_name.lower(),
                                'next':''})
                    pparent_bc.append(['%s za %s' % \
                        (parent_model._meta.verbose_name_plural, unicode(pparent)),\
                            pparent_url_link])
        if pparent_bc:
            breadcrumbs.append(pparent_bc)
        else:
            # Listanje svih parent-a
            parent_list = [parent_model._meta.verbose_name_plural,\
                reverse('admin:%s_%s_changelist' % (parent_app_label, \
                    parent_model_name.lower()))]
            breadcrumbs.append([parent_list])
            
        if parent.id:
            # tekući parent - ako imamo pparent i ako je jedinstven 
            # renderovaćemo kao link (ovo će biti u većini slučajeva)
            # Ukoliko pparent nije jedinstven ne možemo unapred znati u kom
            # kontekstu bi korisnik hteo da edituje parent element pa ćemo ovu
            # stavku renderovati bez linka.
            if len(pparent_bc)==1:
                parent_change_link = '%s%d/' % (\
                        reverse('admin:%s_%s_parent_context' %\
                            (pparent._meta.app_label, \
                                pparent._meta.module_name), \
                        kwargs={'parent_id': pparent.id, \
                                'child_app_label': parent_app_label, \
                                'field_name': pparent_link[1], \
                                'child_model_name': parent_model_name.lower(), \
                                'next':''}), parent.id)
            elif len(pparent_bc)==0:
                # Ako nema pparent-a onda ćemo renderovati link za standardni
                # changeview.
                parent_change_link = reverse('admin:%s_%s_change' % \
                    (parent_app_label, parent_model_name.lower()), \
                        args=(parent.id,))
                
            else:
                parent_change_link = ''
            breadcrumbs.append([[unicode(parent), parent_change_link]])
            
            if child_app_label and field_name and child_model_name:
                child_model = self.model
                
                context_url = reverse('admin:%s_%s_parent_context' % \
                        (parent._meta.app_label, parent._meta.module_name), \
                        kwargs={'parent_id': parent.id, \
                                'child_app_label': child_app_label, \
                                'field_name': field_name, \
                                'child_model_name': child_model_name,\
                                'next':''})
                
                # listview za child-ove
                breadcrumbs.append([[\
                    unicode(child_model._meta.verbose_name_plural), \
                        context_url]])
                if child_id:
                    child_id = int(child_id)
                    child = child_model.objects.get(pk=child_id)
                    breadcrumbs.append([[\
                        unicode(child),\
                        '%s%d/' % (context_url, child_id)]])
                    
        
        if not appending:
            breadcrumbs[-1][-1][-1]=''
        return breadcrumbs
