# -*- coding: utf-8 -*-
from django.db.models.loading import get_model
from django.core.urlresolvers import reverse

def get_stripped_path(path):
    base_url = reverse('admin:index')
    if path.startswith(base_url):
       return path[len(base_url):]
    return None
        
def get_child_model(request):
    path = get_stripped_path(request.path)
    if path:
        path_parts = path.split('/')
        if len(path_parts)>=6: # Ako imamo child
            child_app_label = path_parts[3]
            child_model_name = path_parts[5]
            child_model = get_model(child_app_label, \
                child_model_name)
            return child_model
    return None

def get_parent_model(request):
    '''
    '''
    path = get_stripped_path(request.path)
    if path:
        parent_app_label, parent_model_name = path.split('/')[0:2]
        parent_model = get_model(parent_app_label, parent_model_name)
        return parent_model
    return None
    

def get_parent(request):
    '''
    Na osnovu URL-a vraća parent objekat.
    '''
    path = get_stripped_path(request.path)
    if path:
        parent_app_label, parent_model_name, parent_id = path.split('/')[0:3]
        try:
            parent_id = int(parent_id)
        except:
            return None
        
        parent_model = get_model(parent_app_label, parent_model_name)
        parent = parent_model and parent_model.objects.get(pk=parent_id)
        
        return parent
    return None
