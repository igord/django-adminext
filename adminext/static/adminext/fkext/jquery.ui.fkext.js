/*
 * fkext
 * 
 * Extended foreign key combo.
 * 
 * Author: Igor Dejanović, igor.dejanovic@gmail.com
 * 
 * Depends:
 *  jquery.ui.core.js
 *  jquery.ui.widget.js
 * 
 */

function update_add_link(add_link, dep_input){
  var parent_id = $('#'+dep_input).val(),
      add_link_node = $('#'+add_link),
      url_template = add_link_node.data('url_template');
  if(parent_id){
    if(url_template){
      url = url_template.replace(/\/0\//, '/'+parent_id+'/')
      add_link_node.attr('href', url);
      add_link_node.show();
    }
  }else{
    add_link_node.hide();
  }
}

(function( $, undefined ) {

$.widget( "ui.fkext", {
  options: {
    source: null,
    parent_id: null,
    initial_value: null,
  },
  _create: function() {
    var self=this,
        id_attr = this.element.attr("id");
    this.url_element = $("#link_" + id_attr)
    this.add_element = $("#add_" + id_attr)
    self.current_value = self.options.initial_value;
    if(typeof this.options.parent_id == "string"){
        this.parent_element = $('#'+this.options.parent_id);
        this.parent_element.
          bind("change.fkext", function(event){
                self._fetch();
          });
    }

  
    // -----------
    
    this.response = function() { // This is needed for _response method to get self reference
      return self._response.apply( self, arguments );
    };

    this.element
      .addClass( "ui-fkext" )
      .bind("change.fkext", function( event ){
//           self.current_value = self.element.val();
          self._update_link();
      });
    
    // Initial load
    this._fetch();

  },

  destroy: function() {
    this.element
      .removeClass( "ui-fkext" )
    $.Widget.prototype.destroy.call( this );
  },
  
  _update_link: function() {
    var selected = $(this.element).find(':selected');
    if(selected && selected.data('url')){
        this.url_element.attr('href', selected.data('url'));
        this.url_element.show();
    }else{
        this.url_element.hide();
    }
  },

  _fetch: function() {
        var self = this;

        var parent_id = this.options.parent_id;
        if('parent_element' in this){
            parent_id = this.parent_element.val();
        };
        
        if(parent_id){
            var data = {
                parent_id: parent_id
            };
            this.element.addClass( "ui-fkext-loading" );
            $.ajax({
                url: this.options.source,
                dataType: "json",
                data: data,
                success: function( data ) {
                    self.response(data);
                }
            });
        }
    },

  _response: function( content ) {
    var self = this;
    this.element.empty();
    options = this.element.attr('options');
    options[options.length] = new Option('------------', '', true, true);
    if ( content && content.length ) {
        $.each(content, function(index, item){
            var selected = item.value==self.current_value,
            option = new Option(item.label, item.value, selected, selected);
            $(option).data('url', item.url);
            options[options.length] = option;
        });
    }
    this.element.removeClass( "ui-fkext-loading" );
    this.element.change();
  },

  
});

}( jQuery ));

