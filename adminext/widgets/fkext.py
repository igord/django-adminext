# -*- coding: utf-8 -*-
# ForeignKeyExt django widget
# Author: Igor R. Dejanovic, igor.dejanovic@gmail.com
# See LICENSE for licensing information.

from base64 import urlsafe_b64encode, urlsafe_b64decode

from django import forms
from django.db.models import get_model
from django.forms.widgets import flatatt
from django.utils.encoding import smart_unicode
from django.core.urlresolvers import reverse
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.simplejson import JSONEncoder
from django.conf import settings
from django.utils import simplejson
from django.http import HttpResponse, \
                        HttpResponseBadRequest, HttpResponseForbidden
from django.utils.translation import ugettext as _


def uri_b64encode(s):
    return urlsafe_b64encode(s).strip('=')

def uri_b64decode(s):
    if len(s) % 4:
        return urlsafe_b64decode(s + '=' * (4 - len(s) % 4))
    else:
        return urlsafe_b64decode(s)

def fkext_ajax_view(request, key):
    
    if request.method=="GET":
        if request.GET.has_key('parent_id'):
            parent_id = int(request.GET['parent_id'])
            model, parent_field_name = uri_b64decode(str(key)).split(':')
            model = get_model(*model.split('.'))
            filter_kwargs = { '%s__id' % parent_field_name: parent_id}
            objs = [{'label': unicode(o),
                         'value': o.id,
                         'url': hasattr(o, 'get_absolute_url') and o.get_absolute_url() or ''}
                        for o in model.objects.filter(**filter_kwargs)]
            return HttpResponse(simplejson.dumps(objs))
    return HttpResponseForbidden()
        

class ForeignKeyExt(forms.Widget):

    class Media:
        URL = "%sadminext" % settings.STATIC_URL
        if settings.DEBUG:
            js = (  "%sjquery/jquery.js" % settings.STATIC_URL,
                    "%sjquery/ui/jquery.ui.core.js" % settings.STATIC_URL,
                    "%sjquery/ui/jquery.ui.widget.js" % settings.STATIC_URL,
                    "%s/fkext/jquery.ui.fkext.js" % URL,                    
                    "%s/fkext/fkext_RelatedObjectLookups.js" % URL,                    
            )
        else:
            js = (  "%sjquery/jquery.min.js" % settings.STATIC_URL,
                    "%sjquery/ui/jquery.ui.core.js" % settings.STATIC_URL,
                    "%sjquery/ui/jquery.ui.widget.js" % settings.STATIC_URL,
                    "%s/fkext/jquery.ui.fkext.js" % URL,                    
                    "%s/fkext/fkext_RelatedObjectLookups.js" % URL,                    
            )
            
        # Styling
        css = { 'all':
            ( "%sjquery/themes/current/jquery.ui.core.css" % settings.STATIC_URL,
              "%sjquery/themes/current/jquery.ui.theme.css" % settings.STATIC_URL,
              "%s/fkext/fkext.css" % URL,             
            )
        }

    def __init__(self, model, parent_field_name, parent=None,
                 options={}, attrs={}):
        '''
        @model              - referenced model
        @parent_field_name  - parent field name of referenced model
        @parent             - field name or id of parent object 
        '''
        self.model = model
        self.parent_field_name = parent_field_name
        self.parent = parent
        self.attrs = attrs
        self.options = options
        
        self.add_link = ''

        self.params = []
        
        self.urlkey = uri_b64encode("%s.%s:%s" % \
                              (model._meta.app_label, model._meta.module_name,
                                parent_field_name))
    
        # Ajax request source
        if isinstance(parent, str):
            parent_id = 0
        else:
            parent_id = parent
        source = "'%s'" % reverse('admin:fkext_ajax_view',
                            kwargs={ 'key': self.urlkey})
        self.params.append("source:%s" % source)

        if isinstance(self.parent, str): # parent is field name
            self.params.append("parent_id:'id_%s'" % self.parent)
        elif parent is not None: # parent is object id
            self.params.append("parent_id:%d" % self.parent)
            
        if len(options) > 0:
            self.options = JSONEncoder().encode(options)
            
        #self.attrs.update(attrs)
    
    def render_js(self, field_id, value):
        
        if value:
            self.params.append("initial_value:%s" % value)
        options = ''
        if self.options:
            options += ',%s' % self.options

        params = ', '.join(self.params)
        
        return u'''$('#%(field_id)s').fkext({%(params)s}%(options)s);
            ''' % {
                'field_id': field_id,
                'params': params,
                'options': options,
            }

    def render(self, name, value=None, attrs=None):
        if value is None: value = ''
        final_attrs = self.build_attrs(attrs, name=name)

        if not self.attrs.has_key('id'):
            final_attrs['id'] = 'id_%s' % name
        
        add_link_html = u''
        ## Uklonjena mogućnost dodavanja preko fk na zahtev ATS-a.
        #add_link_html = u'''
        #    <a href="%(add_link)s" class="add-another" id="add_id_%(name)s"
        #        style="display:none;" onclick="return showAddAnotherPopup(this);">
        #        <img src="%(media_url)sfkext/icon_addlink.gif"
        #            width="10" height="10" alt="%(alt)s"/>
        #    </a>
        #''' % {
        #    'add_link': self.add_link,
        #    'name': name,
        #    'alt': _(u'Додај још један'),
        #    'media_url': settings.MEDIA_URL,
        #}
            
        js = u'''<script type="text/javascript"><!--//
                %s//--></script>''' % self.render_js(final_attrs['id'], value)

        
        return mark_safe(u'''<select%(attrs)s></select>
        <a id="link_id_%(name)s" class="fkext-link" target="_blank" href="" style="display:none;"></a>
        %(add_link_html)s
        %(js)s
        ''' % {
            'name': name,
            'attrs' : flatatt(final_attrs),
            'add_link_html': add_link_html,
            'js' : js,
        })


