django-adminext
===============

Django_ admin extensions focused on easier data entry and navigation.

.. _Django: http://www.djangoproject.com/

Features
--------

- Parent-child navigation from ``changelist`` and ``change`` views to their
  child instances ``changelist`` views.
- Adding models instances in the context of the parent instance.
- Breadcrumbs navigation for going up the ancestor links.
- Autocomplete foreign key component with support for quickly access of referenced
  model instance and adding new instance in context of its parent.
- ForeignKeyExt combo box component that is context aware and has support for
  quickly access of referenced model instance.

Instalation
-----------

Download_ and unpack archive or `clone source`_ from bitbucket.org and execute::

    python setup.py install

AdminExt uses `django.contrib.admin` and `django.contrib.staticfiles`.

Put `adminext` in INSTALLED_APPS **before** `django.contrib.admin`::

    INSTALLED_APPS = (
        'adminext',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.sites',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'django.contrib.admin',
    )

Admin must be enabled.

`STATIC_URL` in `settings.py` should be::

    STATIC_URL = '/static/'

`ADMIN_MEDIA_PREFIX` should be::

    ADMIN_MEDIA_PREFIX = '/static/admin/'



.. _Download: https://bitbucket.org/igord/django-adminext/downloads
.. _`clone source`: https://bitbucket.org/igord/django-adminext/src



Usage
-----

These are the models used in this section as an example::

    class Country(models.Model):
        name = models.CharField(max_length=100)
        code = models.CharField(max_length=3)
        
        class Meta:
            verbose_name_plural = _('Countries')
            
        def __unicode__(self):
            return self.name
        
        
    class City(models.Model):
        country = models.ForeignKey(Country)
        name = models.CharField(max_length=100)
        is_capital = models.BooleanField(default=False)
        
        class Meta:
            verbose_name_plural = _('Cities')

        def __unicode__(self):
            return self.name  


ModelAdminExt
~~~~~~~~~~~~~
      
Admin models must inherits from ModelAdminExt class like this::

    from django.contrib import admin
    from adminext.admin import ModelAdminExt
    from models import *

    class CountryAdmin(ModelAdminExt):
        child_links = (
            ('countries.city', 'country'),
        )


child_links
~~~~~~~~~~~

ModelAdminInherited classes with `child_links` list will automatically get column
`Child items` with links to children change list form.

`child_links` list of ModelAdminExt inherited class contains tuples whose first
element is child model reference in the form `app_label.lowecase_model_name` and
the second element is child model field name of type ForeignKeyField that reference
this model (Country in this case).

Child forms will knew its parent context so, for example, only siblings will be 
displayed and adding new child will implicitly set foreign key field to current
parent model instance.

There will be extended breadcrumbs navigation to climb up ancestors path.


freeze_fields
~~~~~~~~~~~~~

`freeze_fields` lists models fields that will retain its value on multiple object
entry with 'Save and add another' option.

For example::

    class CityAdmin(ModelAdminExt):
        freeze_fields = ('country',)

If change form is called without parent context (e.g. through main index) than
this enables us to enter multiple cities for the same country.
Of course, this could also be achieved by navigating from country using
`Child items` link to cities and that issuing Add operation but `freeze_fields`
works also for non-ForeignKey fields.

    
TODO
----

- More examples and documentation for widgets.
- Easier declarative widget specification.
- Breadcrumbs for children with multiple ancestors.
- Better integration with django.contrib.auth
- More widgets (e.g. ajax many-to-many).
- Tests.
- Clean up comments.

  



